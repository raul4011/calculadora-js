window.onload = start;

function start() {
    
  let primerValor = 0;
  let segundoValor = 0;
  let operacion = "";
  let memoriaVirtual = 0;
  let resultadoFinal = 0;

  //eventos de botones
  document.getElementById("uno").addEventListener("click", mostrar);
  document.getElementById("dos").addEventListener("click", mostrar);
  document.getElementById("tres").addEventListener("click", mostrar);
  document.getElementById("cuatro").addEventListener("click", mostrar);
  document.getElementById("cinco").addEventListener("click", mostrar);
  document.getElementById("seis").addEventListener("click", mostrar);
  document.getElementById("siete").addEventListener("click", mostrar);
  document.getElementById("ocho").addEventListener("click", mostrar);
  document.getElementById("nueve").addEventListener("click", mostrar);
  document.getElementById("cero").addEventListener("click", mostrar);
  //eventos de operaciones
  document.getElementById("suma").addEventListener("click", suma);
  document.getElementById("resta").addEventListener("click", resta);
  document.getElementById("producto").addEventListener("click", producto);
  document.getElementById("division").addEventListener("click", division);
  //evento de borrar y calcular
  document.getElementById("reset").addEventListener("click", reset);
  document.getElementById("igual").addEventListener("click", igual);
  document.getElementById("mem1").addEventListener("click", memoria);
  document.getElementById("mem2").addEventListener("click", memoria2);
  document.getElementById("memT").addEventListener("click", memoriaT);
  document.getElementById("memX").addEventListener("click", borrarMemoria);
  //span que mostrara el resultado en un display
  let resultado = document.getElementById("resultado");
  let display = document.getElementById("display");

  document.getElementsByClassName("btn").a;

  function mostrar() {
    let num = this.value;
    resultado.innerHTML += num;
    display.innerHTML += num;
  }

  function suma() {
    primerValor = resultado.innerHTML;
    console.log(primerValor);
    primerValor = display.innerHTML;
    operacion = "+";
    display.innerHTML = primerValor + operacion;

    borrar();
  }

  function resta() {
    primerValor = resultado.innerHTML;

    primerValor = display.innerHTML;
    operacion = "-";
    display.innerHTML = primerValor + operacion;

    borrar();
  }

  function producto() {
    primerValor = resultado.innerHTML;

    primerValor = display.innerHTML;
    operacion = "*";
    display.innerHTML = primerValor + operacion;
    borrar();
  }

  function division() {
    primerValor = resultado.innerHTML;

    primerValor = display.innerHTML;
    operacion = "/";
    display.innerHTML = primerValor + operacion;
    borrar();
  }

  function borrar() {
    resultado.innerHTML = "";
  }

  function reset() {
    resultado.innerHTML = "";
    display.innerHTML = "";
    primerValor = 0;
    segundoValor = 0;
    operacion = "";
  }

  function igual() {
    segundoValor = resultado.innerHTML;
    console.log(segundoValor);
    calcular();
  }

  function calcular() {
    switch (operacion) {
      case "+":
        resultadoFinal = parseInt(primerValor) + parseInt(segundoValor);
        console.log(resultadoFinal);
        break;
      case "-":
        resultadoFinal = parseInt(primerValor) - parseInt(segundoValor);
        console.log(resultadoFinal);
        break;
      case "*":
        resultadoFinal = parseInt(primerValor) * parseInt(segundoValor);
        break;
      case "/":
        if (segundoValor != 0) {
          resultadoFinal = parseInt(primerValor) / parseInt(segundoValor);
        } else {
          alert("no existe la division x cero");
          reset();
        }
        break;
    }
    reset();
    resultado.innerHTML = resultadoFinal;
  }

  function memoria() {
    memoriaVirtual += parseInt(resultado.innerHTML);
    console.log(`memoriaPostiva:  ${memoriaVirtual}`);
  }

  function memoria2() {
    memoriaVirtual -= parseInt(resultado.innerHTML);
    console.log(`memoriaNegativa:  ${memoriaVirtual}`);
  }
  function memoriaT() {
    console.log(`la memoria total es: ${memoriaVirtual}`);
    display.innerHTML = `La memoria total es ${memoriaVirtual}`;
  }

  function borrarMemoria() {
    memoriaVirtual = 0;
    console.log(`La memoria total es: ${memoriaVirtual}`);
    display.innerHTML = `Memoria en 0`;
  }
}
